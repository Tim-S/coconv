
#[cfg(test)]
mod tests {
    use crate::coconv::{Wgs84Coordinate, EarthCenteredEarthFixed, RangeAzimuthElevation, EastNorthUp, Rad, Degree};

    #[test]
    fn degree_to_rad() {
        assert_eq!(f64::from(Rad::from(Degree::from(90.0))), std::f64::consts::FRAC_PI_2);
    }

    #[test]
    fn ecef_from_wgs84() {
        let ecef : EarthCenteredEarthFixed = EarthCenteredEarthFixed::from(Wgs84Coordinate{
            altitude: 999.9564,
            latitude: Degree::from(0.0).into(),
            longitude: Degree::from(45.0).into()
        });
        assert_eq!(4510731.0,ecef.x.round());
        assert_eq!(4510731.0,ecef.y.round());
        assert_eq!(0.0, ecef.z.round());
    }

    #[test]
    fn wgs84_from_ecef() {
        let lla = Wgs84Coordinate::from(EarthCenteredEarthFixed{
            x: 4510731.0, y: 4510731.0, z:0.0
        });
        assert_eq!(f64::from(Degree::from(lla.latitude)), 0.0);
        assert_eq!(f64::from(Degree::from(lla.longitude)), 45.0);
        assert!((999.9564- lla.altitude).abs() < 1.0);
    }

    #[test]
    fn conversions_between_east_north_up_and_range_azimuth_elevation() {
        let rae = RangeAzimuthElevation::from(EastNorthUp{
            east: 100.0, north: 50.0, up: 25.0
        });
        assert!((f64::from(Degree::from(rae.azimuth))-63.434948822922).abs() < 1e-12);
        assert!((f64::from(Degree::from(rae.elevation))-12.604382648379183).abs() < 1e-12);
        assert!((rae.range - 114.564392373896).abs() < 1e-12);

        let enu = EastNorthUp::from(rae);
        assert!((enu.east - 100.0).abs() < 1e-12);
        assert!((enu.north - 50.0).abs() < 1e-12);
        assert!((enu.up - 25.0).abs() < 1e-12);
    }

    #[test]
    fn translate_earth_centered_earth_fixed_by_east_north_up(){
        let origin = Wgs84Coordinate{
            altitude: 0.0,
            latitude: Rad::from(Degree::from(0.0)),
            longitude: Rad::from(Degree::from(0.0))
        };
        let ecef = EarthCenteredEarthFixed::from(origin);

        let enu = EastNorthUp{ east: 0.0, north: 100.0, up: 0.0 };
        let mut translated_ecef =  ecef;
        translated_ecef.translate(&enu,origin.latitude, origin.longitude);

        let lla = Wgs84Coordinate::from(translated_ecef);

        assert!(lla.longitude.value.abs() < 1e-12);
        assert!(lla.latitude.value.abs() > 0.0); //Should have move northwards
        assert!(lla.altitude > 0.0); //Should have move northwards

        // Check some more if the values are plausible:
        let surface_distance_dist_in_meter : f64 = Degree::from(lla.latitude).value*111000.0; // 1° is ~111km
        assert!(surface_distance_dist_in_meter > enu.north); // surfaceDistance i greater than direct distance
        assert!((surface_distance_dist_in_meter - enu.north).abs() < 1.0); // 100 meter means the surface-distance is about ~0.3 < 1 greater
    }

    #[test]
    fn get_bearing_from_two_earth_centered_earth_fixed_coordinates(){
        let origin = Wgs84Coordinate{
            altitude: 0.0,
            latitude: Rad::from(Degree::from(0.0)),
            longitude: Rad::from(Degree::from(0.0))
        };

        let ecef_origin = EarthCenteredEarthFixed::from(origin);
        let mut ecef_target =  ecef_origin;
        let enu = EastNorthUp{ east: 0.0, north: 100.0, up: 0.0 };
        ecef_target.translate(&enu, origin.latitude, origin.longitude);
        let result = ecef_origin.bearing(&ecef_target, origin.latitude, origin.longitude);
        assert_eq!(result.east, enu.east);
        assert_eq!(result.north, enu.north);
        assert_eq!(result.up, enu.up);
    }
}

mod coconv;