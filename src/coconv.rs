#![allow(non_upper_case_globals)]

use crate::coconv::constants::RAD_PER_DEGREE;
use crate::coconv::constants::wgs84::{a, b};

mod constants{
    pub const RAD_PER_DEGREE : f64  = 0.017453292519943295;

    #[allow(dead_code)]
    pub mod wgs84{
        pub const a : f64 =  6378137.0  ; // m
    pub const f : f64 = 1.0 / 298.257223563;
        pub const e : f64 = 0.08181919084;
        pub const b : f64 = 6356752.314; // m
    pub const A : f64 = 42697.673; //m
    pub const B : f64 = 42841.312; //m
    }
}

#[derive(Debug, PartialOrd, PartialEq, Copy, Clone)]
pub struct Rad {
    pub value: f64,
}
impl From<f64> for Rad {
    fn from(f: f64) -> Self {
        Rad { value: f }
    }
}
impl From<Degree> for Rad {
    fn from(f: Degree) -> Self {
        Rad { value: f.value*RAD_PER_DEGREE }
    }
}
impl From<Rad> for f64 {
    fn from(f: Rad) -> Self {
        f.value
    }
}

#[derive(Debug, PartialOrd, PartialEq, Copy, Clone)]
pub struct Degree {
    pub value: f64,
}
impl From<f64> for Degree {
    fn from(f: f64) -> Self {
        Degree { value: f }
    }
}
impl From<Rad> for Degree {
    fn from(f: Rad) -> Self {
        Degree { value: f.value/RAD_PER_DEGREE}
    }
}
impl From<Degree> for f64 {
    fn from(f: Degree) -> Self {
        f.value
    }
}

//    const a_inverse : f64 = 1.56785594288739799723e-7;
    const a_squared : f64 = a * a;
// m
const b_squared : f64 = b * b;
// m
const a_squared_inverse : f64 = 2.45817225764733181057e-14;
const l : f64 = 3.34718999507065852867e-3;
const l_squared : f64 = 1.12036808631011150655e-5;
const one_minus_e_squared : f64 = 9.93305620009858682943e-1;
//    const one_minus_e_squared_div_a_squared : f64 = 2.44171631847341700642e-14;
    const one_minus_e_squared_div_b : f64 = 1.56259921876129741211e-7;
#[allow(dead_code)]
const H_min : f64 = 2.25010182030430273673e-14;
const one_div_third_root_of_two : f64 = 7.93700525984099737380e-1;
//    const a_squared_div_c : f64 = 7.79540464078689228919e-7;
//    const b_squared_div_c_squared : f64 = 1.48379031586596594555e-2;
//    const inverse_a_squared_div_c : f64 = 1.28280704604842283426943374325629070879430662617774263e6;

#[derive(Debug, Copy, Clone)]
pub struct EarthCenteredEarthFixed {
    pub x: f64, pub y: f64, pub z: f64
}

#[derive(Debug, Copy, Clone)]
pub struct Wgs84Coordinate {
    pub latitude: Rad, pub longitude: Rad, pub altitude: f64
}

impl From<Wgs84Coordinate> for EarthCenteredEarthFixed {
    #[allow(non_snake_case)]
    fn from(coord: Wgs84Coordinate) -> Self {
        let phi : f64 = coord.latitude.into();
        let lambda : f64 = coord.longitude.into();
        let h = coord.altitude;
        let N = a_squared / (a_squared*phi.cos().powi(2) + b_squared*phi.sin().powi(2)).sqrt();
        let d = (N + h)*phi.cos();

        EarthCenteredEarthFixed {
            x : d*lambda.cos(),
            y : d*lambda.sin(),
            z : (N*one_minus_e_squared + h)*phi.sin()
        }
    }
}

impl From<EarthCenteredEarthFixed> for Wgs84Coordinate {
    #[allow(non_snake_case)]
    fn from(coord: EarthCenteredEarthFixed) -> Self {

        let w_squared = coord.x.powi(2) + coord.y.powi(2);
        let m = w_squared * a_squared_inverse;
        let n = (coord.z*one_minus_e_squared_div_b).powi(2);
        let p = (m + n - (4.0 * l_squared)) / 6.0;
        let G = m*n*l_squared;
        let H = (2.0*p.powi(3))+G;
//            if(H < Wgs84Constants.H_min)
//              abort();
        let C = (H+G+2.0*(H*G).sqrt()).powf(1.0/3.0)*one_div_third_root_of_two;
        let i = -(2.0*l_squared + m + n) / 2.0;
        let P = p.powi(2);
        let beta = i/3.0 - C - P/C;
        let k = l_squared*(l_squared - m - n);
        let t = ((beta.powi(2) - k).sqrt()-(beta+i)/2.0).sqrt() - (m-n).signum()*((beta - i) / 2.0).abs().sqrt();
        let F = t.powi(4) + 2.0*i*t.powi(2) + 2.0*l*(m - n)*t + k;
        let dF = 4.0*t.powi(3) + 4.0*i*t + 2.0*l*(m - n);
        let delta_t = -(F / dF);
        let u = t + delta_t + l;
        let v = t + delta_t - l;
        let w = w_squared.sqrt();
        let phi = f64::atan2(coord.z*u, w*v);
        let delta_w = w * (1.0 - 1.0/u);
        let delta_z = coord.z * (1.0 - one_minus_e_squared/v);

        Wgs84Coordinate {
            latitude: Rad::from(phi),
            longitude: Rad::from(f64::atan2(coord.y, coord.x)),
            altitude: (u-1.0).signum()*(delta_w.powi(2) + delta_z.powi(2)).sqrt()
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct EastNorthUp {
    pub east: f64, pub north: f64, pub up: f64
}

#[derive(Debug, Copy, Clone)]
pub struct RangeAzimuthElevation {
    pub range: f64, pub azimuth: Rad, pub elevation: Rad
}

impl From<RangeAzimuthElevation> for EastNorthUp {
    fn from(rae: RangeAzimuthElevation) -> Self {
        EastNorthUp {
            north : rae.range * rae.elevation.value.cos() * rae.azimuth.value.cos(),
            east : rae.range * rae.elevation.value.cos() * rae.azimuth.value.sin(),
            up : rae.range * rae.elevation.value.sin()
        }
    }
}

impl  From<EastNorthUp> for RangeAzimuthElevation{
    fn from(enu: EastNorthUp) -> Self {
        let _v = enu.east.powi(2) + enu.north.powi(2);
        let range = (_v + enu.up.powi(2)).sqrt();
        RangeAzimuthElevation{
            range,
            elevation: Rad {value: f64::atan2(enu.up, _v.sqrt())},
            azimuth: Rad {value: f64::atan2(range*enu.east, range*enu.north)}
        }
    }
}

impl EarthCenteredEarthFixed {
    pub fn translate(&mut self, enu: &EastNorthUp, latitude_of_origin: Rad, longitude_of_origin: Rad) {
        let phi = latitude_of_origin.value;
        let lambda = longitude_of_origin.value;
        let sin_phi = phi.sin();
        let cos_phi = phi.cos();
        let sin_lambda = lambda.sin();;
        let cos_lambda = lambda.cos();

        let east = enu.east;
        let north = enu.north;
        let up = enu.up;

        let dx = (-sin_lambda * east) + (-cos_lambda*sin_phi*north) + (cos_lambda*cos_phi*up);
        let dy = (cos_lambda*east) + (-sin_lambda*sin_phi*north) + (sin_lambda*cos_phi*up);
        let dz = (cos_phi * north) + (sin_phi * up);

        self.x += dx;
        self.y += dy;
        self.z += dz;
    }

    pub fn bearing(&self, target: &EarthCenteredEarthFixed,
                   latitude_of_origin: Rad, longitude_of_origin: Rad) -> EastNorthUp {
        let dx = target.x - self.x;
        let dy = target.y - self.y;
        let dz = target.z - self.z;

        let phi = latitude_of_origin.value;
        let lambda = longitude_of_origin.value;
        let sin_phi = phi.sin();
        let sin_lambda = lambda.sin();
        let cos_phi = phi.cos();
        let cos_lambda = lambda.cos();

        EastNorthUp {
            east: (-sin_lambda*dx) + (cos_lambda*dy),
            north: (-cos_lambda*sin_phi*dx) + (-sin_lambda*sin_phi*dy) + (cos_phi*dz),
            up: (cos_lambda*cos_phi*dx) + (sin_lambda*cos_phi*dy) + (sin_phi*dz)
        }
    }
}

